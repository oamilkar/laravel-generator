<?php

namespace OAmilkar\Generator;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class GeneratorTemplatesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views/views', 'generator-templates');

        $this->publishes([
            __DIR__.'/../views/views/common' => resource_path('views/vendor/generator-templates/common'),
        ], 'generator-views');

        $this->publishes([
            __DIR__.'/../views/views/templates' => resource_path('views/vendor/generator-templates/templates'),
        ], 'generator-templates');

        Paginator::defaultView('generator-templates::common.paginator');
        Paginator::defaultSimpleView('generator-templates::common.simple_paginator');

        Blade::directive('ocb', function () {
            return '<?php echo "{{ " ?>';
        });

        Blade::directive('ccb', function () {
            return '<?php echo " }}" ?>';
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
