<?php

namespace OAmilkar\Generator;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use OAmilkar\Generator\Commands\API\APIControllerGeneratorCommand;
use OAmilkar\Generator\Commands\API\APIGeneratorCommand;
use OAmilkar\Generator\Commands\API\APIRequestsGeneratorCommand;
use OAmilkar\Generator\Commands\API\TestsGeneratorCommand;
use OAmilkar\Generator\Commands\APIScaffoldGeneratorCommand;
use OAmilkar\Generator\Commands\Common\MigrationGeneratorCommand;
use OAmilkar\Generator\Commands\Common\ModelGeneratorCommand;
use OAmilkar\Generator\Commands\Common\RepositoryGeneratorCommand;
use OAmilkar\Generator\Commands\Publish\GeneratorPublishCommand;
use OAmilkar\Generator\Commands\Publish\PublishTablesCommand;
use OAmilkar\Generator\Commands\Publish\PublishUserCommand;
use OAmilkar\Generator\Commands\RollbackGeneratorCommand;
use OAmilkar\Generator\Commands\Scaffold\ControllerGeneratorCommand;
use OAmilkar\Generator\Commands\Scaffold\RequestsGeneratorCommand;
use OAmilkar\Generator\Commands\Scaffold\ScaffoldGeneratorCommand;
use OAmilkar\Generator\Commands\Scaffold\ViewsGeneratorCommand;
use OAmilkar\Generator\Common\FileSystem;
use OAmilkar\Generator\Common\GeneratorConfig;
use OAmilkar\Generator\Generators\API\APIControllerGenerator;
use OAmilkar\Generator\Generators\API\APIRequestGenerator;
use OAmilkar\Generator\Generators\API\APIRoutesGenerator;
use OAmilkar\Generator\Generators\API\APITestGenerator;
use OAmilkar\Generator\Generators\FactoryGenerator;
use OAmilkar\Generator\Generators\MigrationGenerator;
use OAmilkar\Generator\Generators\ModelGenerator;
use OAmilkar\Generator\Generators\RepositoryGenerator;
use OAmilkar\Generator\Generators\RepositoryTestGenerator;
use OAmilkar\Generator\Generators\Scaffold\ControllerGenerator;
use OAmilkar\Generator\Generators\Scaffold\MenuGenerator;
use OAmilkar\Generator\Generators\Scaffold\RequestGenerator;
use OAmilkar\Generator\Generators\Scaffold\RoutesGenerator;
use OAmilkar\Generator\Generators\Scaffold\ViewGenerator;
use OAmilkar\Generator\Generators\SeederGenerator;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $configPath = __DIR__.'/../config/laravel_generator.php';
            $this->publishes([
                $configPath => config_path('laravel_generator.php'),
            ], 'laravel-generator-config');

            $this->publishes([
                __DIR__.'/../views/generator' => resource_path('views/vendor/laravel-generator'),
            ], 'laravel-generator-templates');
        }

        $this->registerCommands();
        $this->loadViewsFrom(__DIR__.'/../views/generator', 'laravel-generator');

        View::composer('*', function ($view) {
            $view_data = [
                'name' => $view->getName(),
                'path' => $view->getPath(),
                'data' => $view->getData(),
            ];
            if(!isset($view_data['data']['config'])) {
                // \Log::debug('GeneratorServiceProvider view::composer(*)', ['view' => $view_data]);
                $view->with(['config' => app(GeneratorConfig::class)]);
            }
        });

        Blade::directive('tab', function () {
            return '<?php echo infy_tab() ?>';
        });

        Blade::directive('tabs', function ($count) {
            return "<?php echo infy_tabs($count) ?>";
        });

        Blade::directive('nl', function () {
            return '<?php echo infy_nl() ?>';
        });

        Blade::directive('nls', function ($count) {
            return "<?php echo infy_nls($count) ?>";
        });

        Blade::directive('php_tag', function () {
            return '<?php echo "<?php".PHP_EOL ?>';
        });

    }

    private function registerCommands()
    {
        if (!$this->app->runningInConsole()) {
            // return;
        }

        $this->commands([
            APIScaffoldGeneratorCommand::class,

            APIGeneratorCommand::class,
            APIControllerGeneratorCommand::class,
            APIRequestsGeneratorCommand::class,
            TestsGeneratorCommand::class,

            MigrationGeneratorCommand::class,
            ModelGeneratorCommand::class,
            RepositoryGeneratorCommand::class,

            GeneratorPublishCommand::class,
            PublishTablesCommand::class,
            PublishUserCommand::class,

            ControllerGeneratorCommand::class,
            RequestsGeneratorCommand::class,
            ScaffoldGeneratorCommand::class,
            ViewsGeneratorCommand::class,

            RollbackGeneratorCommand::class,
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/laravel_generator.php', 'laravel_generator');

        $this->app->singleton(GeneratorConfig::class, function () {
            return new GeneratorConfig();
        });

        $this->app->singleton(FileSystem::class, function () {
            return new FileSystem();
        });

        $this->app->singleton(MigrationGenerator::class);
        $this->app->singleton(ModelGenerator::class);
        $this->app->singleton(RepositoryGenerator::class);

        $this->app->singleton(APIRequestGenerator::class);
        $this->app->singleton(APIControllerGenerator::class);
        $this->app->singleton(APIRoutesGenerator::class);

        $this->app->singleton(RequestGenerator::class);
        $this->app->singleton(ControllerGenerator::class);
        $this->app->singleton(ViewGenerator::class);
        $this->app->singleton(RoutesGenerator::class);
        $this->app->singleton(MenuGenerator::class);

        $this->app->singleton(RepositoryTestGenerator::class);
        $this->app->singleton(APITestGenerator::class);

        $this->app->singleton(FactoryGenerator::class);
        $this->app->singleton(SeederGenerator::class);
    }
}
