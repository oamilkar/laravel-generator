<!-- Field: {{ strtoupper($fieldName) }} -->
@@php
@if($config->options->localized)
    ${{ $fieldName }}_config = [
        'onText' => __('app.switch_text.yes'),
        'onColor' => 'success',
        'offText' => __('app.switch_text.no'),
        'offColor' => 'danger',
    ];
@else
    ${{ $fieldName }}_config = [
        'onText' => 'Yes',
        'onColor' => 'success',
        'offText' => 'No',
        'offColor' => 'danger',
    ];
@endif
@@endphp
<{{ "x-adminlte-input-switch" }} fgroup-class="col-sm-6" enable-old-support
    name="{{ $fieldName }}"
@if($config->options->localized)
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
@else
    label="{{ $fieldTitle }}:"
@endif
    :config="${{ $fieldName }}_config"
    :checked="old('{{ $fieldName }}', optional(${{ $config->modelNames->camel }} ?? null)->{{ $fieldName }})"
    />
