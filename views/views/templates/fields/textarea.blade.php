<!-- Field: {{ strtoupper($fieldName) }} -->
@if($config->options->localized)
<{{ "x-adminlte-textarea" }} fgroup-class="col-sm-6" enable-old-support
    rows=5
    name="{{ $fieldName }}"
    label="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}:"
    placeholder="@{!! __('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}') !!}"
    >@{!! old('{{ $fieldName }}', optional(${{ $config->modelNames->camel }} ?? null)->{{ $fieldName }}) !!}</{{ "x-adminlte-textarea" }}>
@else
<{{ "x-adminlte-textarea" }} fgroup-class="col-sm-6" enable-old-support
    rows=5
    name="{{ $fieldName }}"
    label="{{ $fieldTitle }}:"
    placeholder="{{ $fieldTitle }}"
    >@{!! old('{{ $fieldName }}', optional(${{ $config->modelNames->camel }} ?? null)->{{ $fieldName }}) !!}</{{ "x-adminlte-textarea" }}>
@endif