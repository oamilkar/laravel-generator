<!-- Field: {{ strtoupper($fieldName) }} -->
@if($config->options->localized)
<div class="form-group col-sm-6">
    @{!! html()->checkbox("{{ $fieldName }}") !!}
    @{!! html()->label({{"__"}}('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}'), '{{ $fieldName }}') !!}
</div>
@else
<div class="form-group col-sm-6">
    @{!! html()->checkbox("{{ $fieldName }}") !!}
    @{!! html()->label('{{ $fieldTitle }}', '{{ $fieldName }}') !!}
</div>
@endif