<!-- {{ $fieldTitle }} Field -->
<div class="form-group col-sm-12">
@if($config->options->localized)
    @{!! html()->label(__('models/{{ $config->modelNames->camelPlural }}.fields.{{ $fieldName }}'), '{{ $fieldName }}') !!}
@else
    @{!! html()->label('{{ $fieldTitle }}'), '{{ $fieldName }}' !!}
@endif
    {!! $radioButtons !!}
</div>