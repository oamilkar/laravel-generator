@@section('plugins.Datatables', true)

@@push('css')
    @@include('layouts.datatables_css')
@@endpush

<div class="card-body px-4">
    @{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
</div>

@@push('js')
    @{!! $dataTable->scripts() !!}
@@endpush
