@@extends('layouts.app')

@@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>
@if($config->options->localized)
                        @@lang('crud.edit') @@lang('models/{!! $config->modelNames->camelPlural !!}.singular')
@else
                        Edit {{ $config->modelNames->human }}
@endif
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        <div class="card">
            @{!! html()->modelForm(${{ $config->modelNames->camel }}, 'PUT', route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->dashedPlural }}.update', ${{ $config->modelNames->camel }}))->open() !!}

            <{{ "x-card-section" }} title="">
                <div class="row">
                    @@include('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.form_fields')
                </div>
            </{{ "x-card-section" }}>

            <div class="card-footer">
                @{!! html()->submit('Save')->class('btn btn-primary') !!}
                <a href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural !!}.index') }}" class="btn btn-default">@if($config->options->localized) @@lang('crud.cancel') @else Cancel @endif</a>
            </div>

            @{!! html()->closeModelForm() !!}
        </div>
    </div>
@@endsection
