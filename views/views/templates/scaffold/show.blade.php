@@extends('layouts.app')

@@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
@if($config->options->localized)
                        @@lang('models/{!! $config->modelNames->camelPlural !!}.singular') @@lang('crud.detail')
@else
                        {{ $config->modelNames->human }} Details
@endif
                    </h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right"
                       href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.edit', ${!! $config->modelNames->camel  !!}) }}">
@if($config->options->localized)
                            @@lang('crud.edit')
@else
                            Edit
@endif
                    </a>
                    <a class="btn btn-default float-right"
                       href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->dashedPlural  !!}.index') }}">
@if($config->options->localized)
                            @@lang('crud.back')
@else
                            Back
@endif
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">

            <{{ "x-card-section" }} title="">
                <div class="row">
                    @@include('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.show_fields')
                </div>
            </{{ "x-card-section" }}>

        </div>
    </div>
@@endsection
