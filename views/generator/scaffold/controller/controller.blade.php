@php
    echo php_tag();
@endphp

namespace {{ $config->namespaces->controller }};

// Framework classes
use Illuminate\Http\Request;

// Vendor classes

// Application classes
use {{ $config->namespaces->app }}\Http\Controllers\AppBaseController;
use {{ $config->namespaces->request }}\Create{{ $config->modelNames->name }}Request;
use {{ $config->namespaces->request }}\Update{{ $config->modelNames->name }}Request;

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};
@if(config('laravel_generator.tables') == 'datatables')
use {{ $config->namespaces->dataTables }}\{{ $config->modelNames->name }}DataTable;
@endif

class {{ $config->modelNames->name }}Controller extends AppBaseController
{
    /**
     * Constructor of the controller
     */
    public function __construct()
    {
        // uncomment to use model policy
        $this->authorizeResource({{ $config->modelNames->name }}::class, '{{ $config->modelNames->snake }}');
    }

    /**
     * Display a listing of the {{ $config->modelNames->name }}.
     */
    {!! $indexMethod !!}

    /**
     * Show the form for creating a new {{ $config->modelNames->name }}.
     */
    public function create()
    {
        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.create');
    }

    /**
     * Store a newly created {{ $config->modelNames->name }} in storage.
     */
    public function store(Create{{ $config->modelNames->name }}Request $request)
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }} = {{ $config->modelNames->name }}::create($input);

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->dashedPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.saved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} saved successfully',
@endif
        ]);
    }

    /**
     * Display the specified {{ $config->modelNames->name }}.
     */
    public function show({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.show')->with([
            '{{ $config->modelNames->camel }}' => ${{ $config->modelNames->camel }},
        ]);
    }

    /**
     * Show the form for editing the specified {{ $config->modelNames->name }}.
     */
    public function edit({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        return view('{{ $config->prefixes->getViewPrefixForInclude() }}{{ $config->modelNames->snakePlural }}.edit')->with([
            '{{ $config->modelNames->camel }}' => ${{ $config->modelNames->camel }},
        ]);
    }

    /**
     * Update the specified {{ $config->modelNames->name }} in storage.
     */
    public function update(Update{{ $config->modelNames->name }}Request $request, {{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }}->fill($input);
        ${{ $config->modelNames->camel }}->save();

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->dashedPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.updated', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} updated successfully',
@endif
        ]);
    }

    /**
     * Remove the specified {{ $config->modelNames->name }} from storage.
     *
     * @throws \Exception
     */
    public function destroy({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        ${{ $config->modelNames->camel }}->delete();

        return redirect(route('{{ $config->prefixes->getRoutePrefixWith('.') }}{{ $config->modelNames->dashedPlural }}.index'))->withMessage([
            'type' => 'success',
@if($config->options->localized)
            'text' => __('messages.deleted', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'text' => '{{ $config->modelNames->human }} deleted successfully',
@endif
        ]);
    }

    protected function findOrFail($id): {{ $config->modelNames->name }}
    {
        return {{ $config->modelNames->name }}::findOr($id, function() {
@if($config->options->localized)
            $message = __('messages.not_found', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]);
@else
            $message = '{{ $config->modelNames->human }} not found';
@endif
            abort(404, $message);
        });
    }
}
