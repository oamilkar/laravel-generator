@php
    echo php_tag();
@endphp

namespace {{ $config->namespaces->seeder }};

use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};

class {{ $config->modelNames->plural }}Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $default = {{ $config->modelNames->name }}::firstOrCreate(
        //     ['name' => 'default {{ $config->modelNames->name }}'],
        //     {{ $config->modelNames->name }}::factory()->make()->toArray()
        // );

        $numItems = 10;
        {{ $config->modelNames->name }}::factory()->count($numItems)->create();
    }
}
