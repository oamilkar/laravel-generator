@php
    echo php_tag();
@endphp

namespace {{ $config->namespaces->apiController }};

// Framework classes
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

// Vendor classes

// Application classes
use {{ $config->namespaces->app }}\Http\Controllers\AppBaseController;
use {{ $config->namespaces->apiRequest }}\Create{{ $config->modelNames->name }}APIRequest;
use {{ $config->namespaces->apiRequest }}\Update{{ $config->modelNames->name }}APIRequest;

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};

{!! $docController !!}
class {{ $config->modelNames->name }}APIController extends AppBaseController
{
    /**
     * Constructor of the controller
     */
    public function __construct()
    {
        // uncomment to use model policy
        $this->authorizeResource({{ $config->modelNames->name }}::class, '{{ $config->modelNames->snake }}');
    }

    {!! $docIndex !!}
    public function index(Request $request)
    {
        $query = {{ $config->modelNames->name }}::query();

        return JsonResource::collection($query->paginate())->additional([
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->humanPlural }} retrieved successfully',
@endif
        ]);
    }

    {!! $docStore !!}
    public function store(Create{{ $config->modelNames->name }}APIRequest $request)
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }} = {{ $config->modelNames->name }}::create($input);

        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.saved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} saved successfully',
@endif
        ];
    }

    {!! $docShow !!}
    public function show({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} retrieved successfully',
@endif
        ];
    }

    {!! $docUpdate !!}
    public function update(Update{{ $config->modelNames->name }}APIRequest $request, {{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }}->fill($input);
        ${{ $config->modelNames->camel }}->save();

        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.updated', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} updated successfully',
@endif
        ];
    }

    {!! $docDestroy !!}
    public function destroy({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        ${{ $config->modelNames->camel }}->delete();

        return [
            'data' => compact('id'),
@if($config->options->localized)
            'message' => __('messages.deleted', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} deleted successfully',
@endif
        ];
    }

    protected function findOrFail($id): {{ $config->modelNames->name }}
    {
        return {{ $config->modelNames->name }}::findOr($id, function() {
@if($config->options->localized)
            $message = __('messages.not_found', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]);
@else
            $message = '{{ $config->modelNames->human }} not found';
@endif
            abort(404, $message);
        });
    }
}
