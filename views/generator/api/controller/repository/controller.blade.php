@php
    echo php_tag();
@endphp

namespace {{ $config->namespaces->apiController }};

// Framework classes
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

// Application classes
use {{ $config->namespaces->apiRequest }}\Create{{ $config->modelNames->name }}APIRequest;
use {{ $config->namespaces->apiRequest }}\Update{{ $config->modelNames->name }}APIRequest;
use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};
use {{ $config->namespaces->repository }}\{{ $config->modelNames->name }}Repository;
use {{ $config->namespaces->app }}\Http\Controllers\AppBaseController;

{!! $docController !!}
class {{ $config->modelNames->name }}APIController extends AppBaseController
{
    public function __construct(private {{ $config->modelNames->name }}Repository ${{ $config->modelNames->camel }}Repository)
    {
        // uncomment to use model policy
        $this->authorizeResource({{ $config->modelNames->name }}::class, '{{ $config->modelNames->snake }}');
    }

    {!! $docIndex !!}
    public function index(Request $request)
    {
        $query = $this->{{ $config->modelNames->camel }}Repository->search(
            $request->except(['page', 'skip', 'limit'])
        );

        return JsonResource::collection($query->paginate())->additional([
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->humanPlural }} retrieved successfully',
@endif
        ]);
    }

    {!! $docStore !!}
    public function store(Create{{ $config->modelNames->name }}APIRequest $request)
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }} = $this->{{ $config->modelNames->camel }}Repository->create($input);

        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.saved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} saved successfully',
@endif
        ];
    }

    {!! $docShow !!}
    public function show({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.retrieved', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} retrieved successfully',
@endif
        ];
    }

    {!! $docUpdate !!}
    public function update(Update{{ $config->modelNames->name }}APIRequest $request, {{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        $input = $request->all();

        ${{ $config->modelNames->camel }} = $this->{{ $config->modelNames->camel }}Repository->update(${{ $config->modelNames->camel }}, $input);

        return [
            'data' => ${{ $config->modelNames->camel }},
@if($config->options->localized)
            'message' => __('messages.updated', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} updated successfully',
@endif
        ];
    }

    {!! $docDestroy !!}
    public function destroy({{ $config->modelNames->name }} ${{ $config->modelNames->camel }})
    {
        $this->{{ $config->modelNames->camel }}Repository->delete(${{ $config->modelNames->camel }});

        return [
            'data' => compact('id'),
@if($config->options->localized)
            'message' => __('messages.deleted', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]),
@else
            'message' => '{{ $config->modelNames->human }} deleted successfully',
@endif
        ];
    }

    protected function findOrFail($id): {{ $config->modelNames->name }}
    {
        return $this->{{ $config->modelNames->camel }}Repository->query()->findOr($id, function() {
@if($config->options->localized)
            $message = __('messages.not_found', ['model' => __('models/{{ $config->modelNames->camelPlural }}.singular')]);
@else
            $message = '{{ $config->modelNames->human }} not found';
@endif
            abort(404, $message);
        });
    }
}
