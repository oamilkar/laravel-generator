@php
    echo php_tag();
@endphp

namespace {{ $config->namespaces->repository }};

use {{ $config->namespaces->model }}\{{ $config->modelNames->name }};
use {{ $config->namespaces->app }}\Repositories\BaseRepository;

class {{ $config->modelNames->name }}Repository extends BaseRepository
{
    protected $fieldSearchable = [
        {!! $fieldSearchable !!}
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return {{ $config->modelNames->name }}::class;
    }


    public function create(array $input)
    {
        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    public function update($id, array $input)
    {
        $model = ($id instanceof {{ $config->modelNames->name }}) ? $id : $this->query()->findOrFail($id);

        $model->fill($input);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        $model = ($id instanceof {{ $config->modelNames->name }}) ? $id : $this->query()->findOrFail($id);

        return $model->delete();
    }
}
